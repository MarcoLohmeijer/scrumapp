<?php

return [
    'project' => [
        'yourProjects' => 'Your projects',
        'joinedProjects' => 'Joined projects',
        'showProject' => [
            'name' => 'Name',
            'startDate' => 'Start date',
            'endDate' => 'End date',
            'team' => 'Team: ',
            'sprints' => 'Sprints'
        ]
    ],
    'navbar' => [
        'home' => 'Home',
        'teams' => 'Teams',
        'assignedTickets' => 'Assigned tickets',
    ],
    'sprintReview' => [
        'finishedStoryPoints' => 'Finished story points: ',
        'unfinishedStoryPoints' => 'Unfinished story points: ',
        'finishedBusinessValue' => 'Finished business value: ',
        'unfinishedBusinessValue' => 'Unfinished business points: ',
        'review' => 'review'
    ],

];
