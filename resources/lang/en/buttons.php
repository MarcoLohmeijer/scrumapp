<?php

return [
    'sidenav' => [
        'home' => 'Homepage',
        'backlog' => 'Backlog',
        'scrumBoard' => 'Scrum board',
        'productBacklog' => 'Product backlog',
        'definitionOfDone' => 'Definition of done',
        'addTicket' => 'Add ticket',
    ],
    'newItems' => [
        'addTeam' => 'Add team',
        'newTeam' => 'Create new team',
        'newSprint' => 'New sprint',
        'newItem' => 'Add new item',
        'newProject' => 'Create new project',
        'newRetrospectiveitem' => 'New Retrospectiveitem',
        'newSprintReview' => 'New Sprint Review'
    ],
    'edit' => 'Edit',
    'remove' => 'Remove',
    'delete' => 'Delete',
    'removeTeam' => 'Remove team',
    'back' => 'Back',
    'board' => 'Board',
    'backlog' => 'Backlog',
    'show' => 'Show',
    'dod' => 'Definition of done',
    'retro' => 'Retro',
    'review' => 'Review'
];
