@extends('layouts.app')

@section('content')
    @include('components.model')

    <body style="height: 100%">
    <div class="text-center"><h1 class="p-3">Defenition Of Done</h1></div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-7"></div>
        <div class="col-sm-2">
            <form>
                <button type="button" class="btn btn-primary m-2" data-toggle="modal" data-target="#myModal">voeg toe
                </button>
                <input type="hidden" value="{{request()->route('dod')}}">
            </form>
        </div>
        <div class="col-sm-1"></div>
    </div>
    @foreach($dodItems as $doDItem)
        <div class="row h-100">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="container border-left border-right border-top rounded p5">
                    <div class="card m-3">
                        <div class="card-body">
                            <p class="card-text">{{$doDItem['text']}}</p>
                            <div class="btn-group float-right">
                                <form method="get" action="/edit_dod_item/{{$doDItem->id}}">
                                    @csrf
                                    <button type="submit" class="btn btn-primary">pas aan</button>
                                </form>
                                <form method="post" action="/delete_dod_item/{{$doDItem->id}}">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger">verwijder</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    @endforeach
    </body>
@endsection
