@extends ('layouts.app')

@section ('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <h1 class="text-center">Assigned items</h1>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">@lang('texts.project.showProject.name')</div>
                        <div class="col-sm-4">sprint</div>
                        <div class="col-sm-2">Created at</div>
                        <div class="col-sm-2">updated at</div>
                        <div class="col-sm-2"></div>
                    </div>
                    <hr>
                    @foreach ($items as $item)
                        <div class="row p-1">
                            <div class="col-sm-2">
                                {{$item->title}}
                            </div>
                            <div class="col-sm-4">
                                {{$item->sprint->name}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($item->created_at))}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($item->updated_at))}}
                            </div>
                            <div class="col-sm-2">
                                <div class="btn-group float-right">
                                    <a href="{{route('project.sprint.item.show', ['project' => $item->sprint->project->id, 'sprint' => $item->sprint->id, 'item' => $item->id])}}"
                                       class="btn btn-primary w-auto float-right" role="button">
                                        <i class="fas fa-eye"></i> @lang('buttons.show')
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
