@extends('layouts.app')

@section('content')
    <div class="modal-body">
        <form action="/update/dod_item/{{$definitionOfDoneItem->id}}" method="post">
            @csrf
            {{method_field('PUT')}}
            <input type="text" name="text">
            <button class="btn btn-primary" type="submit" name="submit">submit</button>
        </form>
    </div>
@endsection
