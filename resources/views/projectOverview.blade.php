@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        @include('components.modalAddTeamToProject')
        @include('components.modalCreateNewSprint')
        @include('components.modalCreateNewTeam')
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <form class="float-left" action="{{ route('home') }}">
                    <button class="btn">
                        <i class="fas fa-angle-left"></i> @lang('buttons.back')
                    </button>
                </form>
                <h1 class="text-center">{{$project->name}}</h1>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-10">
        <div class="card">
            <div class="card-body">

                {{-- description block--}}
                <div class="row">
                    <div class="col-sm-12">
                        <p>{{$project->description}}</p>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    {{-- Teams block--}}
                    <div class="col-sm-4">
                        <h4><b>teams active in project</b>

                        <div class="btn-group float-right">
                            <!-- Button trigger modal -->
                            <button class="btn btn-success w-auto" data-toggle="modal" data-target="#createNewTeam">
                                <i class="fas fa-users"></i> @lang('buttons.newItems.newTeam')
                            </button>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary w-auto" data-toggle="modal" data-target="#addTeamModal">
                                <i class="fas fa-plus"></i>
                                @lang('buttons.newItems.addTeam')
                            </button>
                        </div>
                        </h4><br>

                        <div id="accordion">
                        @foreach($project->teams as $team)
                            <button class="btn btn-outline-dark m-1 p-3" type="button" data-toggle="collapse" data-target="#collapse-{{ $team->id }}" aria-expanded="false" aria-controls="collapseExample">
                                @lang('texts.project.showProject.team'){{$team->name}}
                            </button>

                            <div class="collapse" id="collapse-{{ $team->id }}">
                                <div class="card card-body">
                                    @foreach($team->users()->get() as $user)
                                        <p>{{$user->username}}</p>
                                    @endforeach
                                    <form action="{{route('project.deleteTeam', ['project' => $project->id, 'team' => $team->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary w-auto float-right">@lang('buttons.removeTeam')</button>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                        </div>

                    </div>

                    {{-- Sprints block--}}
                    <div class="col-sm-8">
                        <h4><b>@lang('texts.project.showProject.sprints')</b>
                            <div class="btn-group float-right">
                                <a href="{{route('project.backlog.index', ['project' => $project->id])}}" class="btn btn-primary w-auto float-right" role="button">
                                    <i class="fas fa-list-alt"></i> @lang('buttons.backlog')
                                </a>
                                <button type="button" class="btn btn-success w-auto float-right" data-toggle="modal" data-target="#createNewSprint">
                                    <i class="fas fa-plus"></i>
                                    @lang('buttons.newItems.newSprint')
                                </button>
                            </div>
                        </h4><br>

                        <div class="row">
                            <div class="col-sm-4">@lang('texts.project.showProject.name')</div>
                            <div class="col-sm-2">@lang('texts.project.showProject.startDate')</div>
                            <div class="col-sm-2">@lang('texts.project.showProject.endDate')</div>
                            <div class="col-sm-4"></div>
                        </div>
                        <hr>

                        @foreach($sprints as $sprint)
                        <div class="row p-1">
                            <div class="col-sm-4">
                                {{$sprint->name}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($sprint->start_date))}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($sprint->end_date))}}
                            </div>
                            <div class="col-sm-4">
                                <div class="btn-group float-right">
                                    <a data-toggle="modal" data-target="#deleteSprint-{{$sprint->id}}" class="btn btn-danger w-auto text-light float-right" role="button">
                                        <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                    </a>
                                    <a href="{{route('project.sprint.edit', ['project' => $project->id, 'sprint' => $sprint->id])}}" class="btn btn-info w-auto float-right" role="button">
                                        <i class="fas fa-edit"></i> @lang('buttons.edit')
                                    </a>
                                    <a href="{{route('project.sprint.board.index', ['project' => $project->id, 'sprint' => $sprint->id])}}" class="btn btn-primary w-auto float-right" role="button">
                                        <i class="fas fa-chalkboard"></i> @lang('buttons.board')
                                    </a>
                                </div>
                                @include('components.modalDeleteSprint', ['name' => $sprint->name, 'projectId' => $project->id, 'sprintId' => $sprint->id])
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col-sm-1"></div>
</div>

@endsection
