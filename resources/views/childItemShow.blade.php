@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Backlogitems</h1>
            @foreach($childBacklogItems as $childBacklogItem)
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="text-center">
                            <h1>{{$childBacklogItem->title}}</h1>
                        </div>
                        <div>
                            <p>{{$childBacklogItem->description}}
                            <p>storypoints = {{$childBacklogItem->story_points}}</p>
                            <p>Buisness Value = {{$childBacklogItem->business_value}}</p>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            @endforeach
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
