@extends ('layouts.app')

@section ('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <h1 class="text-center">Teams</h1>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">Team name</div>
                        <div class="col-sm-3">Users</div>
                        <div class="col-sm-3">Projects</div>
                        <div class="col-sm-2">Created at</div>
                        <div class="col-sm-2">updated at</div>
                    </div>
                    <hr>
                    @foreach ($teams as $team)
                        <div class="row p-1">
                            <div class="col-sm-2">
                                {{$team->name}}
                            </div>
                            <div class="col-sm-3">
                                @foreach($team->users()->get() as $user)
                                    {{$user->username}} <br>
                                @endforeach
                            </div>
                            <div class="col-sm-3">
                                @foreach($team->projects()->get() as $project)
                                    {{$project->name}} <br>
                                @endforeach
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($team->created_at))}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($team->updated_at))}}
                            </div>
                        </div>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
