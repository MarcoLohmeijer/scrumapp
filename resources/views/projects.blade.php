@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('components.modalCreateProject')
                    <div class="row">
                        <div class="col-sm-10">
                            <h1>@lang('texts.project.yourProjects')</h1>
                        </div>
                        <div class="col-sm-2">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                @lang('buttons.newItems.newProject')
                            </button>
                        </div>
                    </div>
                <hr>
            </div>

            @foreach($projects as $project)
            <div class="col-md-3 p-2">
                <div class="card h-100">
                    <div class="card-header bg-primary text-light">
                        <h5 class="card-text">{{$project->name}}</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ Str::limit( $project->description , 200)}}</p>
                    </div>
                    <div class="card-footer bg-white">
                        <p class="card-text"><small class="text-muted">{{$project->created_at->diffForHumans()}}</small></p>
                    </div>
                    <a href="{{ route('project.show', $project->id) }}" type="button" class="stretched-link"></a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
@endsection
