@extends ('layouts.app')

@section ('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <form class="float-left" action="{{ route('project.show', ['project' => $project->id]) }}">
                        <button class="btn">
                            <i class="fas fa-angle-left"></i> @lang('buttons.back')
                        </button>
                    </form>
                    <h1 class="text-center">Backlog</h1>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">@lang('texts.project.showProject.name')</div>
                        <div class="col-sm-4">sprint</div>
                        <div class="col-sm-2">Created at</div>
                        <div class="col-sm-2">updated at</div>
                        <div class="col-sm-2"></div>
                    </div>
                    <hr>
                    @foreach ($backlogItems as $backlogItem)
                        <div class="row p-1">
                            <div class="col-sm-2">
                                {{$backlogItem->title}}
                            </div>
                            <div class="col-sm-4">
                                {{$backlogItem->sprint->name}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($backlogItem->created_at))}}
                            </div>
                            <div class="col-sm-2">
                                {{date('d F Y', strtotime($backlogItem->updated_at))}}
                            </div>
                            <div class="col-sm-2">
                                <div class="btn-group float-right">
                                    <a data-toggle="modal" data-target="#deleteItem-{{$backlogItem->id}}" class="btn btn-danger w-auto text-light float-right" role="button">
                                        <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                    </a>
                                    <a href="{{route('project.sprint.item.edit', ['project' => $project->id, 'sprint' => $backlogItem->sprint->id, 'item' => $backlogItem->id, 'page' => 9])}}" class="btn btn-info w-auto float-right" role="button">
                                        <i class="fas fa-edit"></i> @lang('buttons.edit')
                                    </a>
                                </div>
                                @include('components.modalDeleteItem')
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
