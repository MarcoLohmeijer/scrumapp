@extends ('layout')

@section ('toast')
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
        <div class="toast" data-autohide="false" style="padding: 3%">
            <button onclick="hideshow()" class="btn btn-primary" id="show_button" style="margin-bottom: 5%">Add</button>
            <form method="post" action="/backlog" id="formElement" style="display: none">
                @csrf
                <div class="form-group">
                    <label class="label" for="title"></label>
                    <div class="control">
                        <input type="title" class="input" id="title" placeholder="Title">
                    </div>
                </div>
                <div class="form-group">
                    <label class="label" for="body"></label>
                    <div class="control">
                        <input type="body" class="input" id="body" placeholder="Text">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.toast').toast('show');
        });
        var button = document.getElementById('show_button')
        button.addEventListener('click',hideshow,false);

        function hideshow() {
            document.getElementById('formElement').style.display = 'block';
            this.style.display = 'none'
        }
    </script>
@endsection
