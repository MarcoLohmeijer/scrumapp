<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('components.headImports')
</head>
<body>
<div class="app">
    <div id="content">
        @include('components.navbar')
        @include('messages')

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @yield('content')
                    @yield ('toast')
                </div>
            </div>
        </div>
    </div>
</div>

@include('components.footer')

</body>
</html>
