@extends('layouts.app')

@section('content')
<body style="height: 100%">
<div class="text-center"><h1 class="p-3">Product Backlog items</h1></div>

  <div class="row h-100">
        <div class="col-sm-2"></div>
      <div class="col-sm-8">
          <div class="container border-left border-right border-top rounded p5">
              <div class="card m-3">
                  <div class="card-body">
                      <div class="row">
                        <h5 class="card-title col-sm-6">Title</h5>
                        <h6 class="text-right col-sm-4">NAME</h6>
                          <h6 class="text-right col-sm-2">points:66</h6>
                      </div>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus</p>
                  </div>
              </div>

              <div class="card m-3">
                  <div class="card-body">
                      <div class="row">
                          <h5 class="card-title col-sm-6">Title</h5>
                          <h6 class="text-right col-sm-4">NAME</h6>
                          <h6 class="text-right col-sm-2">points:66</h6>
                      </div>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus</p>
                  </div>
              </div>
                <div class="col-sm-2"></div>
          </div>
      </div>
  </div>
</body>
@endsection
