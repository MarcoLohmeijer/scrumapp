@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Backlog-item Verwijderen</h1><hr>
            <div class="row h-100">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3">
                            item ID
                        </div>
                        <div class="col-sm-4">
                           item Naam
                        </div>
                    </div><hr>
                    @foreach($backlogItems as $item)
                        <div class="col-sm-12"><br>
                            <form method="post" action=/deleteItem/delete/{{$item['id']}} >
                                @csrf
                            <div class="row">
                                <div class="col-sm-3">
                                    {{$item['id']}}
                                </div>
                                <div class="col-sm-4">
                                    {{$item['title']}}
                                </div>
                                <div class="btn-group">
                                    <label for="submit"></label>
                                    <input name="submit" type="submit" class="form-control btn btn-primary" value="Verzenden" id="submit">
                                </div>
                            </div>
                            </form>
                            </div>

                                @endforeach
                            <br>
                            <a href="home" class="btn btn-primary float-left" role="button">Terug</a>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
