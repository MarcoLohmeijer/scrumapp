@extends ('layout')

@section ('content')
<div id="banner">
    <div class="container">
        <ul class="staff">
            <li><img src="images/pic01.jpg" alt="" /></li>
            <li><img src="images/pic02.jpg" alt="" /></li>
            <li><img src="images/pic03.jpg" alt="" /></li>
        </ul>
        <div class="title">
            <h2>Etiam rhoncus volutpat</h2>
            <span class="byline">Maecenas pede nisl, elementum eu, ornare ac, malesuada at, erat. Proin gravida orci porttitor accumsan.</span>
        </div>
        <p>Vivamus fermentum nibh in augue. Praesent a lacus at urna congue rutrum. Nulla enim eros, porttitor eu, tempus id, varius non, nibh. Duis enim nulla, luctus eu, dapibus lacinia, venenatis. Vestibulum imperdiet, magna nec eleifend rutrum lectus vestibulum velit, euismod lacinia quam nisl id lorem.</p>
        <ul class="actions">
            <li><a href="#" class="button">Etiam posuere</a></li>
        </ul>
    </div>
</div>
<div id="page" class="container">
    <div class="title">
        <h2>Nulla luctus eleifend</h2>
        <span class="byline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span>
    </div>
    <p> Vivamus fermentum nibh in augue. Praesent a lacus at urna congue rutrum. Nulla enim eros nibh. Duis enim nulla, luctus eu, dapibus lacinia, venenatis id, quam. Vestibulum imperdiet, magna nec eleifend rutrum, nunc lectus vestibulum velit, euismod lacinia quam nisl id lorem. Quisque erat. Vestibulum pellentesque, justo mollis pretium suscipit, justo nulla blandit libero, in blandit augue justo quis nisl. Fusce mattis viverra elit. Fusce quis tortor.</p>
    <ul class="actions">
        <li><a href="#" class="button">Etiam posuere</a></li>
    </ul>
</div>
<div id="featured">
    <div class="container">
        <div class="title">
            <h2>Fusce ultrices fringilla metus</h2>
            <span class="byline">Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue</span>
        </div>
        <p>This is <strong>Soft String</strong>, a free, fully standards-compliant CSS template designed by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>. The photos in this template are from <a href="http://fotogrph.com/"> Fotogrph</a>. This free template is released under the <a href="http://templated.co/license">Creative Commons Attribution</a> license, so you're pretty much free to do whatever you want with it (even use it commercially) provided you give us credit for it. Have fun :) </p>
    </div>
    <ul class="actions">
        <li><a href="#" class="button">Etiam posuere</a></li>
    </ul>
</div>
<div id="extra" class="container">
    <div class="title">
        <h2>Praesent scelerisquet</h2>
        <span class="byline">Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue</span>
    </div>
    <div id="three-column">
        <div class="boxA">
            <div class="box">
                <p>Praesent pellentesque facilisis elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
            </div>
        </div>
        <div class="boxB">
            <div class="box">
                <p>Etiam neque. Vivamus consequat lorem at nisl. Nullam non wisi a sem semper eleifend. Donec mattis.</p>
            </div>
        </div>
        <div class="boxC">
            <div class="box">
                <p> Aenean lectus lorem, imperdiet at, ultrices eget, ornare et, wisi. Pellentesque adipiscing purus.</p></div>
        </div>
    </div>
    <ul class="actions">
        <li><a href="#" class="button">Etiam posuere</a></li>
    </ul>
</div>
<div id="copyright" class="container">
    <p>&copy; Untitled. All rights reserved. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Design by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>.</p>
</div>
@endsection
{{--<!DOCTYPE html>--}}
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
{{--    <head>--}}
{{--        <meta charset="utf-8">--}}
{{--        <meta name="viewport" content="width=device-width, initial-scale=1">--}}

{{--        <title>Laravel</title>--}}

{{--        <!-- Fonts -->--}}
{{--        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}

{{--        <!-- Styles -->--}}
{{--        <style>--}}
{{--            html, body {--}}
{{--                background-color: #fff;--}}
{{--                color: #636b6f;--}}
{{--                font-family: 'Nunito', sans-serif;--}}
{{--                font-weight: 200;--}}
{{--                height: 100vh;--}}
{{--                margin: 0;--}}
{{--            }--}}

{{--            .full-height {--}}
{{--                height: 100vh;--}}
{{--            }--}}

{{--            .flex-center {--}}
{{--                align-items: center;--}}
{{--                display: flex;--}}
{{--                justify-content: center;--}}
{{--            }--}}

{{--            .position-ref {--}}
{{--                position: relative;--}}
{{--            }--}}

{{--            .top-right {--}}
{{--                position: absolute;--}}
{{--                right: 10px;--}}
{{--                top: 18px;--}}
{{--            }--}}

{{--            .content {--}}
{{--                text-align: center;--}}
{{--            }--}}

{{--            .title {--}}
{{--                font-size: 84px;--}}
{{--            }--}}

{{--            .links > a {--}}
{{--                color: #636b6f;--}}
{{--                padding: 0 25px;--}}
{{--                font-size: 13px;--}}
{{--                font-weight: 600;--}}
{{--                letter-spacing: .1rem;--}}
{{--                text-decoration: none;--}}
{{--                text-transform: uppercase;--}}
{{--            }--}}

{{--            .m-b-md {--}}
{{--                margin-bottom: 30px;--}}
{{--            }--}}
{{--        </style>--}}
{{--    </head>--}}
{{--    <body>--}}
{{--        <div class="flex-center position-ref full-height">--}}
{{--            @if (Route::has('login'))--}}
{{--                <div class="top-right links">--}}
{{--                    @auth--}}
{{--                        <a href="{{ url('/home') }}">Home</a>--}}
{{--                    @else--}}
{{--                        <a href="{{ route('login') }}">Login</a>--}}

{{--                        @if (Route::has('register'))--}}
{{--                            <a href="{{ route('register') }}">Register</a>--}}
{{--                        @endif--}}
{{--                    @endauth--}}
{{--                </div>--}}
{{--            @endif--}}

{{--            <div class="content">--}}
{{--                <div class="title m-b-md">--}}
{{--                    Laravel--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </body>--}}
{{--</html>--}}
