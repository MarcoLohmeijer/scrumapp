@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <form class="w-auto float-left" action="{{ route('project.sprint.board.index', ['project' => $project->id, 'sprint' => $sprint->id]) }}">
                <button class="btn btn-dark">
                    <i class="fas fa-arrow-left"></i> @lang('buttons.back')
                </button>
            </form>
            @include('components.modalCreateNewSprintReview')
            <h1 class="text-center"> {{$sprint->name}} @lang('texts.sprintReview.review')
                <button type="button" class="btn btn-success w-auto float-right" data-toggle="modal" data-target="#createNewSprintReview">
                    <i class="fas fa-plus"></i>
                    @lang('buttons.newItems.newSprintReview')
                </button></h1><hr>
            <div class="row">
                <div class="col-sm-3">
                    <h4>@lang('texts.sprintReview.finishedStoryPoints'){{$storyPointsFinished}}</h4>
                </div>
                <div class="col-sm-3">
                    <h4>@lang('texts.sprintReview.unfinishedStoryPoints'){{$storyPointsUnfinished}}</h4>
                </div>
                <div class="col-sm-3">
                    <h4>@lang('texts.sprintReview.finishedBusinessValue'){{$businessValueFinished}}</h4>
                </div>
                <div class="col-sm-3">
                    <h4>@lang('texts.sprintReview.unfinishedBusinessValue'){{$businessValueUnfinished}}</h4>
                </div>
                <div class="col-sm-12"><hr><br></div>
            </div>
            <div class="row">

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <h5>Ended Task</h5><hr>
                        @foreach ($SprintReview as $sprintreview)
                            @if ($sprintreview->type == 0 )
                                <div class="card h-100" style="border: none">
                                    <div class="card-header bg-white">
                                        <h5 class="card-text">{{$sprintreview->head}}</h5>
                                    </div>
                                    <div class="card-body">
                                        {{$sprintreview->text}}
                                    </div>
                                    <div class="card-footer bg-white">
                                        <div class="btn-group float-right">
                                            <button type="button" class="btn btn-info w-auto " data-toggle="modal" data-target="#editSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                            <button type="button" class="btn btn-danger w-auto" data-toggle="modal" data-target="#deleteSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                            </button>
                                            @include('components.modalDeleteSprintReview', ['head' => $sprintreview->head, 'sprint_reviewId' => $sprintreview->id])
                                            @include('components.modalEditSprintReview', ['head' => $sprintreview->head, 'text' => $sprintreview->text, 'sprint_reviewId' => $sprintreview->id, 'sprintId' => $sprintreview->sprint_id])
                                        </div>
                                    </div>
                                </div><br><br>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <h5>Not Ended Task</h5><hr>
                        @foreach ($SprintReview as $sprintreview)
                            @if ($sprintreview->type == 1 )
                                <div class="card h-100" style="border: none">
                                    <div class="card-header bg-white">
                                        <h5 class="card-text">{{$sprintreview->head}}</h5>
                                    </div>
                                    <div class="card-body">
                                        {{$sprintreview->text}}
                                    </div>
                                    <div class="card-footer bg-white">
                                        <div class="btn-group float-right">
                                            <button type="button" class="btn btn-info w-auto " data-toggle="modal" data-target="#editSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                            <button type="button" class="btn btn-danger w-auto" data-toggle="modal" data-target="#deleteSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                            </button>
                                            @include('components.modalDeleteSprintReview', ['head' => $sprintreview->head, 'sprint_reviewId' => $sprintreview->id])
                                            @include('components.modalEditSprintReview', ['head' => $sprintreview->head, 'text' => $sprintreview->text, 'sprint_reviewId' => $sprintreview->id, 'sprintId' => $sprintreview->sprint_id])
                                        </div>
                                    </div>
                                </div><br><br>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <h5>Improvement Point</h5><hr>
                        @foreach ($SprintReview as $sprintreview)
                            @if ($sprintreview->type == 2 )
                                <div class="card h-100" style="border: none">
                                    <div class="card-header bg-white">
                                        <h5 class="card-text">{{$sprintreview->head}}</h5>
                                    </div>
                                    <div class="card-body">
                                        {{$sprintreview->text}}
                                    </div>
                                    <div class="card-footer bg-white">
                                        <div class="btn-group float-right">
                                            <button type="button" class="btn btn-info w-auto " data-toggle="modal" data-target="#editSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                            <button type="button" class="btn btn-danger w-auto" data-toggle="modal" data-target="#deleteSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                            </button>
                                            @include('components.modalDeleteSprintReview', ['head' => $sprintreview->head, 'sprint_reviewId' => $sprintreview->id])
                                            @include('components.modalEditSprintReview', ['head' => $sprintreview->head, 'text' => $sprintreview->text, 'sprint_reviewId' => $sprintreview->id, 'sprintId' => $sprintreview->sprint_id])
                                        </div>
                                    </div>
                                </div><br><br>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <h5>Comment</h5><hr>
                        @foreach ($SprintReview as $sprintreview)
                            @if ($sprintreview->type == 3 )
                                <div class="card h-100" style="border: none">
                                    <div class="card-header bg-white">
                                        <h5 class="card-text">{{$sprintreview->head}}</h5>
                                    </div>
                                    <div class="card-body">
                                        {{$sprintreview->text}}
                                    </div>
                                    <div class="card-footer bg-white">
                                        <div class="btn-group float-right">
                                            <button type="button" class="btn btn-info w-auto " data-toggle="modal" data-target="#editSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                            <button type="button" class="btn btn-danger w-auto" data-toggle="modal" data-target="#deleteSprintReview-{{$sprintreview->id}}">
                                                <i class="fas fa-trash-alt"></i> @lang('buttons.delete')
                                            </button>
                                            @include('components.modalDeleteSprintReview', ['head' => $sprintreview->head, 'sprint_reviewId' => $sprintreview->id])
                                            @include('components.modalEditSprintReview', ['head' => $sprintreview->head, 'text' => $sprintreview->text, 'sprint_reviewId' => $sprintreview->id, 'sprintId' => $sprintreview->sprint_id])
                                        </div>
                                    </div>
                                </div><br><br>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
