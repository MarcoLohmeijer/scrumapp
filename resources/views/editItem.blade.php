@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Item aanpassen</h1>
            <form action="{{route('project.sprint.item.update', ['project' => $project->id, 'sprint' => $sprint->id, 'item' => $item->id])}}" method='post'>
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Naam:</label>
                    <input name="title" value="{{$item['title']}}" type="text" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label>Beschijving:</label>
                    <textarea name="description" type="text" class="form-control" id="description" rows="5" >{{$item['description']}}</textarea>
                </div>

                <div class="form-group">
                    <label>asingnee:</label>
                    <select class="form-control" id="user" name="user_id">
                        @foreach($allUsers as $user)
                            <option value={{$user['id']}}> {{$user['username']}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Type item:</label>
                    <select class="form-control" id="type" name="type_id">
                        @foreach($types as $type)
                            <option value={{$type['id']}}> {{$type['type']}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>sprint:</label>
                    <select class="form-control" id="sprint_id" name="sprint_id">
                        @foreach($sprints as $sprint)
                            <option value={{$sprint['id']}}> {{$sprint['name']}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Punten:</label>
                    <input name="story_points" type="number" value="{{$item['story_points']}}" class="form-control" id="story_points">
                </div>

                <div class="form-group">
                    <label>Business value:</label>
                    <input name="business_value" type="number" value="{{$item['business_value']}}" class="form-control" id="business_value">
                </div>
                @if (isset($_GET['page']) == 9)
                    <input type="hidden" value="9" name="page">
                @endif
                <div class="btn-group">
                    <label for="submit"></label>
                    <input name="submit" type="submit" class="form-control btn btn-primary" value="Verzenden" id="submit">
                </div>
            </form>

        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
