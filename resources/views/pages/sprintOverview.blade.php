@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h1>Sprint 1 overview</h1>
            <hr>
        </div>

        <div class="col-sm-6">
            <h3>totaal aantal punten behaald:</h3>
            <b class="h2">32</b>
            <hr>
        </div>

        <div class="col-sm-6">
            <h3>totaal aantal punten niet behaald:</h3>
            <b class="h2">3</b>
            <hr>
        </div>

        <div class="col-sm-6">
            <div class="box-parent">
                <h3>Finished tickets</h3>
                <div class="content-box border-right border-top border-left">
                    <div class="card">
                        <div class="card-header card-title bg-primary">title ticket</div>
                        <div class="card-body">
                            <p>content of the ticket</p>
                            <i>assignee</i>
                            <b class="card-content-poker-number float-right rounded-circle">12</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box-parent">
                <h3>unfinished tickets</h3>
                <div class="content-box border-right border-top border-left">
                    <div class="card">
                        <div class="card-header card-title bg-primary">title ticket</div>
                        <div class="card-body">
                            <p>content of the ticket</p>
                            <i>assignee</i>
                            <b class="card-content-poker-number float-right rounded-circle">12</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
