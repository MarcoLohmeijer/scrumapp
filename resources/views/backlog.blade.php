@extends ('layouts.app')

@section ('toast')
    <body>
    @foreach ($backlogs as $backlog)
    <div class="container">
        <div class="toast" data-autohide="false">
            <div class="toast-header">
                <strong class="mr-auto text-primary"><a href="/backlog/{{ $backlog->id }}">{{ $backlog->title }}</a></strong>
                <small class="text-muted">5 mins ago</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body">
                {{ $backlog->body }}
            </div>
        </div>
    </div>
    @endforeach
    <script>
        $(document).ready(function(){
            $('.toast').toast('show');
        });
    </script>
    <div class="container">
        <div class="toast" data-autohide="false" style="padding: 3%">
            <button onclick="hideshow()" class="btn btn-primary" id="show_button" style="margin-bottom: 5%">Add</button>
            <form method="post" action="/backlogs" id="formElement" style="display: none">
                @csrf

                <div class="form-group">
                    <label class="label" for="title">
                    <input type="title" class="form-control" id="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label class="label" for="body">
                    <input type="body" class="form-control" id="body" placeholder="Text">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        var button = document.getElementById('show_button')
        button.addEventListener('click',hideshow,false);

        function hideshow() {
            document.getElementById('formElement').style.display = 'block';
            this.style.display = 'none'
        }
    </script>
@endsection
