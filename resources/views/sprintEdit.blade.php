@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <form class="float-left position-fixed" action="{{ route('project.show', ['project' => $project->id]) }}">
            <button class="btn">
                <i class="fas fa-angle-left"></i> Back
            </button>
        </form>
        <h2 class="text-center">{{$sprint->name}}</h2><hr>
    </div>
    <div class="col-sm-3"></div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <form method='post' action='{{route('project.sprint.update', ['project' => $project->id, 'sprint' => $sprint->id])}}' enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-group1">
                        <label for="name"><h10>Sprint Naam:</h10></label>
                        <input type="text" class="form-control" name="name" id="name" required="required"
                               value="{{ $sprint->name }}"/>

                    </div><br>

                    <div class="form-group1">
                        <label for="startdate"><h10>Sprint Start Datum:</h10></label>
                        <input type="text" class="form-control" name="startDate" id="start_date" required="required"
                               value="{{ $sprint->start_date }}"/>
                    </div><br>

                    <div class="form-group1">
                        <label for="enddate"><h10>Sprint Finish Datum:</h10></label>
                        <input type="text" class="form-control" name="endDate" id="end_date" required="required"
                               value="{{ $sprint->end_date }}"/>
                    </div><br>

                    <button type="submit" class="btn btn-primary float-right">Update</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-3"></div>
</div>
@endsection
