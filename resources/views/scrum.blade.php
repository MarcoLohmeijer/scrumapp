@extends ('layouts.app')

@section ('content')
    <div class="row board">

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1">
                    <form class="float-left position-fixed" action="{{ route('project.show', ['project' => $project->id]) }}">
                        <button class="btn">
                            <i class="fas fa-angle-left"></i> Back
                        </button>
                    </form>
                </div>
                <div class="col-sm-8">
                    <h1 class="text-center">{{$sprint->name}} Board</h1>
                </div>
                <div class="col-sm-3">
                    <a href="{{route('project.sprint.retrospectiveitem.index', ['project' => $project->id, 'sprint' => $sprint->id,])}}" class="btn btn-success w-auto float-right" role="button">
                        <i class="fas fa-info"></i> @lang('buttons.retro')
                    </a>
                    <a href="{{route('project.sprint.sprintreview.index', ['project' => $project->id, 'sprint' => $sprint->id,])}}" class="btn btn-dark w-auto float-right" role="button">
                        <i class="fas fa-retweet"></i> @lang('buttons.review')
                    </a>
                    @include('components.modalCreateNewItem')
                    <button type="button" class="btn btn-primary float-right w-auto" data-toggle="modal" data-target="#createNewItem">
                        @lang('buttons.newItems.newItem')
                    </button>
                </div>
            </div>
            <hr>
        </div>

        <div id="state1" class="col box" data-state="1">
            <h4>To do</h4>
            <hr>
            @foreach ($items as $item)
                @if ($item->state == 0 || $item->state == 1)
                    <div id="backlog{{ $item->id }}" data-id="{{ $item->id }}" class="card task p-0" draggable="true">
                        <div class="card-header bg-primary text-light">
                            <p>{{ $item->title }}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>{{ substr($item->description, 0, 70) }}</p>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">storypoints: {{ $item->story_points }} </p>
                                    <p class="float-right">Business value: {{ $item->business_value }}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Type: {{$item->type()->first()->type}}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Assignee: {{$item->user()->first()->username}}</p>
                                    <form action="{{route('project.sprint.item.edit', ['project' => $project->id, 'sprint' => $sprint->id, 'item' => $item->id])}}" method="get">
                                        <button class="btn float-right btn-primary w-auto" type="submit">@lang('buttons.edit')</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div id="state2" class="col box" data-state="2">
            <h4>In progress</h4>
            <hr>
            @foreach ($items as $item)
                @if ($item->state == 2)
                    <div id="backlog{{ $item->id }}" data-id="{{ $item->id }}" class="card task p-0" draggable="true">
                        <div class="card-header bg-primary text-light">
                            <p>{{ $item->title }}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>{{ substr($item->description, 0, 70) }}</p>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">storypoints: {{ $item->story_points }} </p>
                                    <p class="float-right">Business value: {{ $item->business_value }}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Type: {{$item->type()->first()->type}}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Assignee: {{$item->user()->first()->username}}</p>
                                    <form action="{{route('project.sprint.item.edit', ['project' => $project->id, 'sprint' => $sprint->id, 'item' => $item->id])}}" method="get">
                                        <button class="btn float-right btn-primary w-auto" type="submit">@lang('buttons.edit')</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div id="state3" class="col box" data-state="3">
            <h4>Testing</h4>
            <hr>
            @foreach ($items as $item)
                @if ($item->state == 3)
                    <div id="backlog{{ $item->id }}" data-id="{{ $item->id }}" class="card task p-0" draggable="true">
                        <div class="card-header bg-primary text-light">
                            <p>{{ $item->title }}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>{{ substr($item->description, 0, 70) }}</p>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">storypoints: {{ $item->story_points }} </p>
                                    <p class="float-right">Business value: {{ $item->business_value }}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Type: {{$item->type()->first()->type}}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Assignee: {{$item->user()->first()->username}}</p>
                                    <form action="{{route('project.sprint.item.edit', ['project' => $project->id, 'sprint' => $sprint->id, 'item' => $item->id])}}" method="get">
                                        <button class="btn float-right btn-primary w-auto" type="submit">@lang('buttons.edit')</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

        <div id="state4" class="col box" data-state="4">
            <h4>Done</h4>
            <hr>
            @foreach ($items as $item)
                @if ($item->state == 4)
                    <div id="backlog{{ $item->id }}" data-id="{{ $item->id }}" class="card task p-0" draggable="true">
                        <div class="card-header bg-primary text-light">
                            <p>{{ $item->title }}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>{{ substr($item->description, 0, 70) }}</p>
                                    <hr>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">storypoints: {{ $item->story_points }} </p>
                                    <p class="float-right">Business value: {{ $item->business_value }}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Type: {{$item->type()->first()->type}}</p>
                                </div>
                                <div class="col-sm-12">
                                    <p class="float-left">Assignee: {{$item->user()->first()->username}}</p>
                                    <form action="{{route('project.sprint.item.edit', ['project' => $project->id, 'sprint' => $sprint->id, 'item' => $item->id])}}" method="get">
                                        <button class="btn float-right btn-primary w-auto" type="submit">@lang('buttons.edit')</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>
@endsection

