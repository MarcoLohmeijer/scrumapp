@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Sprints Overview</h1><hr>
            <div class="row h-100">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-1">
                            Sprint ID
                        </div>
                        <div class="col-sm-4">
                            Sprint Naam
                        </div>
                        <div class="col-sm-4">
                            Project Naam
                        </div>
                        <div class="col-sm-2">
                            Start Datum
                        </div>
                        <div class="col-sm-1">

                        </div>
                    </div><hr>
                    @foreach($sprints as $sprint)
                <div class="col-sm-12"><br>
                    <div class="row">
                        <div class="col-sm-1">
                            {{$sprint['id']}}
                        </div>
                        <div class="col-sm-4">
                            {{$sprint['name']}}
                        </div>
                        <div class="col-sm-4">
                            {{ $sprint->project['name']}}
                        </div>
                        <div class='col-sm-2'>
                            {{$sprint['start_date']}}
                        </div>
                        <form method='get' action='scrumbord/{{ $sprint->id }}'
                              style="display: inline-block; float: right">
                            @csrf
                            <button type="submit" class="btn-warning">kies</button>
                        </form>
                        @endforeach
                    </div><br>
                    <a href='/admin_overview' class="btn btn-primary float-left" role="button">Terug</a>
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
