@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>child item toevoegen</h1>
            <form action={{route('saveItem')}} method="post">
                @csrf
                <div class="form-group">
                    <label>Naam:</label>
                    <input name="name" type="text" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label>Beschijving:</label>
                    <textarea name="description" type="text" class="form-control" id="description" rows="5" ></textarea>
                </div>

                <div class="form-group">
                    <label>Parent Item:</label>
                    <select class="form-control" id="parent_item" name="parent_item">
                        @foreach($parentItems as $parentItem)
                            <option value={{$parentItem->id}}>{{$parentItem->title}} </option>
                        @endforeach

                    </select>
                </div>

                <div class="form-group">
                    <label>asingnee:</label>
                    <select class="form-control" id="user" name="user">
                        @foreach($users as $user)
                            <option value={{$user->id}}>{{$user->username}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Type item:</label>
                    <select class="form-control" id="type" name="type">
                        @foreach($types as $type)
                            <option value={{$type->id}}>{{$type->type}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Punten:</label>
                    <input name="story_points" type="number" class="form-control" id="story_points">
                </div>

                <div class="form-group">
                    <label>Business value:</label>
                    <input name="business_value" type="number" class="form-control" id="business_value">
                </div>

                <div class="btn-group">
                    <label for="submit"></label>
                    <input name="submit" type="submit" class="form-control btn btn-primary" value="Verzenden" id="submit">
                </div>
            </form>

        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
