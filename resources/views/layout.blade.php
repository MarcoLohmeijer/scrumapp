<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=News+Cycle:400,700" rel="stylesheet" />
    <link href="/css/default.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/fonts.css" rel="stylesheet" type="text/css" media="all" />

    <!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
    @yield('header')
</head>
<body>
<div id="header" class="container">
    <div id="logo">
        <h1><a href="#">Scrum</a></h1>
    </div>
    <div id="menu">
        <ul>
            <li class="{{ Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/" accesskey="1" title="">Homepage</a></li>
            <li class="{{ Request::path() === 'backlog' ? 'current_page_item' : ''}}"><a href="/backlog" accesskey="2" title="">Backlog</a></li>
            <li class="{{ Request::path() === 'scrumbord' ? 'current_page_item' : ''}}"><a href="/scrumbord" accesskey="3" title="">Scrumbord</a></li>
            <li class="{{ Request::path() === 'contact' ? 'current_page_item' : ''}}"><a href="#" accesskey="5" title="">Contact Us</a></li>
        </ul>
    </div>
</div>
   @yield ('content')
   @yield ('toast')
</body>
</html>
