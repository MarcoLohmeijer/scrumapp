@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Team Overview</h1>
            <hr>
            <div class="row h-100">
                <div class="col-sm-5">
                    <h10>Maak een nieuwe team aan</h10>
                    <hr>

                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <h10>Verwijder een team</h10>
                    <hr>
                    <div class="row">
                        <table>
                            <tr>
                                <th class="col-4">Team Naam</th>
                                <th class="col-4">Team Omschrijving</th>
                                <th class="col-4">Edit</th>
                                <th class="col-4">Delete</th>
                            </tr>
                                @foreach($teams as $team)
                                    <tr>
                                        <td>{{ $team->name }}</td>
                                        <td>{{ $team->description }}</td>
                                        <td>
                                            <form method='get' action='{{route('team.edit', ['team' => $team->id])}}' style="display: inline-block; float: right">
                                                <button type="submit" class="btn-warning">edit</button>
                                            </form>
                                        </td>
                                        <td>
                                            <form method='post' action='{{route('team.destroy', ['team' => $team->id]) }}' style="display: inline-block">
                                                @csrf
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn-btn btn-primary float-right" style="">
                                                    verwijderen
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
@endsection
