@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <form class="float-left" action="{{ route('items') }}">
                        <button class="btn">
                            <i class="fas fa-angle-left"></i> @lang('buttons.back')
                        </button>
                    </form>
                    <h1 class="text-center">Backlog</h1>
                </div>
                <div class="col-sm-1"></div>
            </div>
            <hr>
        </div>
    </div>

    <div class="row p-1">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header">
                    {{$item->title}}
                </div>
                <div class="card-body">
                    <p>Sprint name: {{$item->sprint->name}}</p>
                    <p>Created at: {{date('d F Y', strtotime($item->created_at))}}</p>
                    <p>Updated at: {{date('d F Y', strtotime($item->updated_at))}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4"></div>

    </div>

@endsection
