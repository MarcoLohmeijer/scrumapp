@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <form class="w-auto float-left" action="{{ route('project.sprint.board.index', ['project' => $project->id, 'sprint' => $sprint->id]) }}">
                <button class="btn btn-dark">
                    <i class="fas fa-arrow-left"></i> Back
                </button>
            </form>
            @include('components.modalCreateNewRetrospectiveitem')
            <h1 class="text-center">{{$sprint->name}} Retrospective Review
                <button type="button" class="btn btn-success w-auto float-right" data-toggle="modal" data-target="#createNewRetrospectiveitem">
                    <i class="fas fa-plus"></i>
                    @lang('buttons.newItems.newRetrospectiveitem')
                </button></h1><hr>
            <div class="row">
            <div class="col-sm-6">
            <div class="row h-100">
                <div class="col-sm-12">
                    <h3>Good</h3><hr>
                    @foreach ($retrospective_items as $retrospective_item)
                     @if ($retrospective_item->type == 0 )
                    <div class="row">
                        <div class="col-sm-7">
                            {{$retrospective_item->text}}
                        </div><br><br>
                        <div class="col-sm-5">
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-info w-auto float-right" data-toggle="modal" data-target="#editRetrospectiveitem-{{$retrospective_item->id}}">
                                    <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                <button data-toggle="modal" data-target="#deleteRetrospectiveItem-{{$retrospective_item->id}}" class="btn btn-danger w-auto text-light float-right" role="button">
                                    <i class="fas fa-trash-alt"></i> @lang('buttons.delete')</button>
                            </div>
                            @include('components.modalEditRetrospectiveItem', ['text' => $retrospective_item->text, 'retrospective_itemId' => $retrospective_item->id, 'sprintId' => $retrospective_item->sprint_id])
                            @include('components.modalDeleteRetrospectiveItem', ['text' => $retrospective_item->text, 'retrospective_itemId' => $retrospective_item->id])
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            </div>

            <div class="col-sm-6">
            <div class="row h-100">
                <div class="col-sm-12">
                    <h3>Not Good</h3><hr>
                    @foreach ($retrospective_items as $retrospective_item)
                        @if ($retrospective_item->type == 1 )
                    <div class="row">
                        <div class="col-sm-7">
                            {{$retrospective_item->text}}
                        </div><br><br>
                        <div class="col-sm-5">
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-info w-auto float-right" data-toggle="modal" data-target="#editRetrospectiveitem-{{$retrospective_item->id}}">
                                    <i class="fas fa-edit"></i> @lang('buttons.edit')</button>
                                <button data-toggle="modal" data-target="#deleteRetrospectiveItem-{{$retrospective_item->id}}" class="btn btn-danger w-auto text-light float-right" role="button">
                                    <i class="fas fa-trash-alt"></i> @lang('buttons.delete')</button>
                            </div>
                            @include('components.modalEditRetrospectiveItem', ['text' => $retrospective_item->text, 'retrospective_itemId' => $retrospective_item->id, 'sprintId' => $retrospective_item->sprint_id])
                            @include('components.modalDeleteRetrospectiveItem', ['text' => $retrospective_item->text, 'retrospective_itemId' => $retrospective_item->id])
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
