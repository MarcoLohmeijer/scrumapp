@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Update</h1>
            <hr>
            <div class="row h-100">
                <div class="col-sm-5">
                    <form method='post' action='{{route('team.update', ['team' => $team->id])}}' enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div id="container">
                            <div class="form-group1">
                                <label for="name">Team Naam:</label>
                                <input type="text" class="form-control" name="name" id="name" required="required"
                                       value="{{ $team->name }}"/>
                            </div>
                            <br>
                            <div class="form-group1">
                                <label for="description">Team Omschrijving:</label>
                                <input type="text" class="form-control" name="description" id="description"
                                       value="{{ $team->description }}" required="required"/>
                            </div>
                            <br>
                            <div class="form-group1 field" id="content">
                                <div class="select is-multiple control mt-3">
                                    <select
                                        name="users[]"
                                        multiple
                                    >
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->username }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary float-right">Update</button>
                    </form>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <div class="row">
                        <table>
                            <tr>
                                <th>Teamleden</th>
                                <th></th>
                            </tr>
                            @foreach($team->users as $user)
                                <tr>
                                    <form method='post' action='{{route('team.deleteUser', ['team' => $team->id, 'user' => $user->id])}}'>
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <td>
                                            <button type="submit" class="btn-btn btn-primary float-left" style="">x
                                            </button>
                                        </td>
                                        <td>
                                            <a href="#">{{ $user->username }}</a>
                                        </td>
                                    </form>
                                    <form method='post' action='{{route('team.updateRole', ['team' => $team->id, 'user' => $user->id])}}'>
                                        @csrf
                                        @method('PUT')
                                        <td>
                                            <select name="role_id" size="1">
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}"{{ $role->id == $user->pivot->role_id ? " selected" : "" }}>{{ $role->role }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit" class="btn-btn btn-primary float-left" style="">+
                                            </button>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
@endsection
