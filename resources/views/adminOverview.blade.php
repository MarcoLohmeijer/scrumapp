@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <h1>Settings Overview</h1><hr>
            <div class="row h-100">

                <div class="col-sm-12">
                    <h5>Team</h5><hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Team Overview</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Team Toevoegen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Team Editen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Team Verwijderen</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12"><br><br>
                    <h5>Gebruiker</h5><hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Gebruiker Overview</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Gebruiker Toevoegen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Gebruiker Editen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Gebruiker Verwijderen</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12"><br><br>
                <h5>Sprint</h5><hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href='sprints_overview' class="btn btn-primary float-left" role="button">Sprint Overview</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='sprint_aanmaken' class="btn btn-primary float-left" role="button">Sprint Toevoegen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='sprint_edit' class="btn btn-primary float-left" role="button">Sprint Editen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='sprint_verwijderen' class="btn btn-primary float-left" role="button">Sprint Verwijderen</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12"><br><br>
                    <h5>Item</h5><hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Item Overview</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Item Toevoegen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Item Editen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">Item Verwijderen</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12"><br><br>
                    <h5>Definition of Done</h5><hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">DoD Overview</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">DoD Toevoegen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">DoD Editen</a>
                        </div>

                        <div class="col-sm-3">
                            <a href='' class="btn btn-primary float-left" role="button">DoD Verwijderen</a>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
