<nav class="nav flex-column border-right side-bar">
    <ul>
        <li class="{{ Request::path() === '/' ? 'current_page_item' : ''}}"><a href="/" class="btn btn-light border-bottom" accesskey="1" title="">Homepage</a></li>
        <li class="{{ Request::path() === 'backlog' ? 'current_page_item' : ''}}"><a href="/backlog" class="btn btn-light border-bottom" accesskey="2" title="">Backlog</a></li>
        <li class="{{ Request::path() === 'scrumbord' ? 'current_page_item' : ''}}"><a href="/scrumbord/1" class="btn btn-light border-bottom" accesskey="3" title="">Scrumbord</a></li>
        <li class="{{ Request::path() === 'productBacklog' ? 'current_page_item' : ''}}"><a href="/productbacklog" class="btn btn-light border-bottom" accesskey="3" title="">Product Backlog</a></li>
        <li class="{{ Request::path() === 'definitionOfDone' ? 'current_page_item' : ''}}"><a href="/dod" class="btn btn-light border-bottom" accesskey="3" title="">Definition of Done</a></li>
        <li class="{{ Request::path() === 'addTicket' ? 'current_page_item' : ''}}"><a href="/addticket" class="btn btn-light border-bottom" accesskey="3" title="">Add ticket</a></li>
    </ul>
</nav>
