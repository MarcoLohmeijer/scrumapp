<!-- Modal -->
<div class="modal fade" id="createNewSprint" tabindex="-1" role="dialog" aria-labelledby="createNewSprintTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create new sprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action={{route('project.sprint.store', ['project' => $project->id])}} method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Sprint Naam:</label>
                        <input name="name" type="text" class="form-control" id="name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label for="startDate">Sprint Start Datum:</label>
                        <input name="startDate" type="date" class="form-control" id="startDate" required="required" min="2019-01-01" max="2100-12-31"/>
                    </div>
                    <div class="form-group">
                        <label for="finishDate">Sprint Finish Datum:</label>
                        <input name="finishDate" type="date" class="form-control" id="finishDate" required=required min="2019-01-01" max="2100-12-31"/>
                    </div>

                    <button type="submit" class="btn btn-primary float-right">create</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
