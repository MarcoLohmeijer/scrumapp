<!-- Modal -->
<div class="modal fade" id="createNewItem" tabindex="-1" role="dialog" aria-labelledby="createNewItemTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create new item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action={{route('project.sprint.item.store', ['project' => $project->id, 'sprint' => $sprint->id])}} method="post">
                    @csrf
                    <div class="form-group">
                        <label>Naam:</label>
                        <input name="name" type="text" class="form-control" id="name">
                    </div>

                    <div class="form-group">
                        <label>Beschijving:</label>
                        <textarea name="description" type="text" class="form-control" id="description" rows="5" ></textarea>
                    </div>

                    <div class="form-group">
                        <label>asingnee:</label>
                        <select class="form-control" id="user" name="user">
                            @foreach($users as $user)
                                <option value={{$user['id']}}>{{$user['username']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Type item:</label>
                        <select class="form-control" id="type" name="type">
                            @foreach($types as $type)
                                <option value={{$type->id}}>{{$type->type}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>sprint:</label>
                        <select class="form-control" id="sprint_id" name="sprint_id">
                            @foreach($sprints as $sprint)
                                <option value={{$sprint->id}}>{{$sprint->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Punten:</label>
                        <input name="story_points" type="number" class="form-control" id="story_points">
                    </div>

                    <div class="form-group">
                        <label>Business value:</label>
                        <input name="business_value" type="number" class="form-control" id="business_value">
                    </div>

                    <div class="btn-group">
                        <label for="submit"></label>
                        <input name="submit" type="submit" class="form-control btn btn-primary" value="Verzenden" id="submit">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
