<nav class="navbar navbar-expand-sm navbar-light bg-success border-bottom shadow-lg">
    <a class="navbar-brand" href="{{route('home')}}">
        <img src="{{URL::asset('/img/logo.jpg')}}" alt="logo">
        <span class="navbar-brand mb-0 h1">{{ config('app.name', 'Laravel') }}</span>
    </a>

    {{-- items on the left of navbar --}}
    <ul class="navbar-nav nav-tabs">
        @guest
        @else
            <li class="nav-item">
                <a class="nav-link text-dark border-0" href="{{ route('home') }}">@lang('texts.navbar.home')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark border-0" href="{{ route('teams') }}">@lang('texts.navbar.teams')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark border-0" href="{{ route('items') }}">@lang('texts.navbar.assignedTickets')</a>
            </li>
        @endguest
    </ul>

    {{-- items on the right of navbar --}}
    <ul class="navbar-nav  ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link text-dark border-0" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link text-dark border-0" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            {{--Dropdown--}}
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark border-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->username }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item text-dark border-0" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>

</nav>

