<!-- Modal -->
<div class="modal fade" id="deleteSprint-{{$sprintId}}" tabindex="-1" role="dialog" aria-labelledby="deleteSprintTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete sprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete: <b>{{$name}}</b></p>
                <form action={{route('project.sprint.destroy', ['project' => $projectId, 'sprint' => $sprintId])}} method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger float-right w-auto">Delete</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
