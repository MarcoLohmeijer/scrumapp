<!-- Modal -->
<div class="modal fade" id="deleteItem-{{$backlogItem->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteItemTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteItemTitle">Delete item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete: <b>{{$backlogItem->title}}</b></p>
                <form action={{route('project.sprint.item.destroy', ['project' => $project->id, 'sprint' => $backlogItem->sprint->id, 'item' => $backlogItem->id])}} method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger float-right w-auto">Delete</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
