<!-- Modal -->
<div class="modal fade" id="createNewTeam" tabindex="-1" role="dialog" aria-labelledby="createNewTeamTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createNewTeamTitle">Create new team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='post' action='{{route('project.team.store', ['project' => $project->id])}}' enctype="multipart/form-data">
                @csrf

                <div id="container">
                    <div class="form-group1">
                        <label for="name">Team Naam:</label>
                        <input type="text" class="form-control input @error('name') is-danger @enderror" name="name" id="name" required/>
                        @error('name')
                        <p class="help is-danger">{{ $errors->first('name') }}</p>
                        @enderror
                    </div>
                    <br>

                    <div class="form-group1">
                        <label for="description">Team Omschrijving:</label>
                        <input type="text" class="form-control input @error('description') is-danger @enderror" name="description" id="description" required/>
                        @error('description')
                        <p class="help is-danger">{{ $errors->first('description') }}</p>
                        @enderror
                    </div>
                    <br>

                    <div class="form-group1 field" id="content">
                        <div class="select is-multiple control mt-3">
                            <select
                                name="users[]"
                                multiple
                            >
                                @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->username }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <br>

                    <div class="form-group1">
                        <input class="form-check-input" type="checkbox" name="addToProject" value="true" id="addToTeam">
                        <label class="form-check-label" for="addToTeam">
                            Add to the project
                        </label>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary float-right">Create</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
