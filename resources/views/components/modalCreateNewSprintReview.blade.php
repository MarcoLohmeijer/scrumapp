<!-- Modal -->
<div class="modal fade" id="createNewSprintReview" tabindex="-1" role="dialog" aria-labelledby="createNewSprintReviewTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create New Sprint Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action={{route('storesprintreview', ['sprint' => $sprint->id])}} method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Ticket Nr. or Point</label>
                        <input name="head" type="text" class="form-control" id="head" required="required"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea name="text" type="text" class="form-control" id="text" rows="3" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Type:</label>
                        <select class="form-control" name="type" id="type">
                            <option value="0" @if (old('type') == 0) selected @endif>Ended Task</option>
                            <option value="1" @if (old('type') == 1) selected @endif>Not Ended Task</option>
                            <option value="2" @if (old('type') == 2) selected @endif>Improvement Point</option>
                            <option value="3" @if (old('type') == 3) selected @endif>Comment</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Create</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
