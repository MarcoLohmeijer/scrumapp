<!-- Modal -->
<div class="modal fade" id="editRetrospectiveitem-{{$retrospective_item->id}}" tabindex="-1" role="dialog" aria-labelledby="editRetrospectiveitemTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Retrospectiveitem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='post' action='{{route('updateretrospectiveitem', ['retrospective_items' => $retrospective_item->id, 'sprintId' => $retrospective_item->sprint_id])}}' enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-group1">
                        <label for="text"><h10>Text:</h10></label>
                        <input type="" class="form-control" name="retroText" id="retroText" required="required"
                               value={{$text}}>
                    </div><br>

                    <div class="form-group1">
                        <label for="type"><h10>Type:</h10></label>
                        <select class="form-control" name="retroType" id="retroType">
                            <option value="0" @if (old('type') == 0) selected @endif>Good</option>
                            <option value="1" @if (old('type') == 1) selected @endif>Not Good</option>
                        </select>
                    </div><br>

                    <div class="form-group1">
                        <label for="sprintid"><h10></h10></label>
                        <input hidden type="hidden" class="form-control" name="sprintId" id="sprintId" required="required"
                               value={{$sprintId}}>
                    </div><br>

                    <button type="submit" class="btn btn-primary float-right">Update</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
