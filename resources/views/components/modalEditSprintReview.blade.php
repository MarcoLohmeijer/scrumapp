<!-- Modal -->
<div class="modal fade" id="editSprintReview-{{$sprintreview->id}}" tabindex="-1" role="dialog" aria-labelledby="editSprintReviewTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Sprint Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='post' action='{{route('updatesprintreview', ['sprint_reviews' => $sprintreview->id, 'sprintId' => $sprintreview->sprint_id])}}' enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="name">Ticket Nr. or Point</label>
                        <input name="head" type="text" class="form-control" id="head" required="required"
                               value={{$head}}>
                    </div>

                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea name="text" type="text" class="form-control" id="text" rows="3" required="required">{{$text}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="name">Type:</label>
                        <select class="form-control" name="type" id="type">
                            <option value="0" @if (old('type') == 0) selected @endif>Ended Task</option>
                            <option value="1" @if (old('type') == 1) selected @endif>Not Ended Task</option>
                            <option value="2" @if (old('type') == 2) selected @endif>Improvement Point</option>
                            <option value="3" @if (old('type') == 3) selected @endif>Comment</option>
                        </select>
                    </div>

                    <div class="form-group1">
                        <label for="sprintid"><h10></h10></label>
                        <input type="hidden" class="form-control" name="sprintId" id="sprintId" required="required"
                               value={{$sprintId}}>
                    </div><br>

                    <button type="submit" class="btn btn-primary float-right">Update</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
