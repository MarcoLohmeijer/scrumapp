<!-- Modal -->
<div class="modal fade" id="createNewRetrospectiveitem" tabindex="-1" role="dialog" aria-labelledby="createNewRetrospectiveitemTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create new Retrospectiveitem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action={{route('storeretrospectiveitem', ['sprint' => $sprint->id])}} method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Text</label>
                        <input name="text" type="text" class="form-control" id="text" required="required"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Type:</label>
                        <select class="form-control" name="type" id="type">
                            <option value="0" @if (old('type') == 0) selected @endif>Good</option>
                            <option value="1" @if (old('type') == 1) selected @endif>Not Good</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Create</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
