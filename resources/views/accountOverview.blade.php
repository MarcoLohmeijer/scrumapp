@extends('layouts.app')

@section('content')
    <div class="row h-100">
        <div class="col-sm-1"></div>
        <div class="col-sm-5">
            <h1>Account Overview</h1><hr>
            <div class="card">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="form-group1">
                            <label for="usr"><h10>Naam:</h10></label>
                            <input>
                        </div><br>

                        <div class="form-group1">
                            <label for="email"><h10>E-mail:</h10></label>
                            <input>
                        </div><br>

                        <div class="form-group1"><label for="pwd"><h10>Current password:</h10></label>
                            <input>
                        </div><br>

                        <button type="submit" class="btn btn-primary float-left">Change password</button>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </form><br><br>
                </div>
            </div><br>
            <a href="home" class="btn btn-primary float-left" role="button">Terug</a>
        </div>
        <div class="col-sm-1"></div>
    </div>
@endsection
