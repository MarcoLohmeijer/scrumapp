//drop target
const dropTarget = document.querySelector(".board");

//drag cards
const draggables = document.querySelectorAll(".task");

//Setting eventlistener to dragable cards
for (let i=0; i<draggables.length; i++) {
    draggables[i].addEventListener("dragstart", function (ev) {
        ev.dataTransfer.setData("backlogId", ev.target.id);
    });
}

//eventlistener when item is over the board
dropTarget.addEventListener("dragover", function (ev) {
    ev.preventDefault();
});

//eventlistener when item is droped
dropTarget.addEventListener("drop", function (ev) {
    ev.preventDefault();
    if (!ev.target.classList.contains("box")) {
        return;
    }
    //get data for request
    const backlogId = ev.dataTransfer.getData("backlogId");
    const state = $(ev.target).attr("data-state");

    if (!backlogId || !state) {
        return;
    }

    //Ajax request
    $.ajax({
        type: "POST",
        url:  "/~marco/scrumapp/api/updateSprintBoard",
        data: {action: "move", "backlogId": backlogId, "state": state}
    }).done(function(data) {
        if (data === "OK") {
            ev.target.appendChild(document.getElementById(backlogId));
        }
    }).fail(function(ctx) {
        alert(ctx.responseText);
        console.log(ctx.responseText);
    });
});
