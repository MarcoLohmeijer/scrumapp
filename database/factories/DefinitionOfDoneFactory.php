<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DefinitionOfDone;
use Faker\Generator as Faker;

$factory->define(DefinitionOfDone::class, function (Faker $faker) {
    return [
        'team_id' => factory('App\Team')->create()->id,
        'project_id' => factory('App\Project')->create()->id
    ];
});
