<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ChildItem;
use Faker\Generator as Faker;

$factory->define(ChildItem::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->text,
        'story_points' => $faker->randomDigit,
        'business_value' => $faker->randomDigit,
        'state' => 0,
        'item_id' => factory('App\Item')->create()->id,
        'type_id' => factory('App\Type')->create()->id,
        'user_id' => factory('App\User')->create()->id
    ];
});
