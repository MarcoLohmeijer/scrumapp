<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Sprint;
use Faker\Generator as Faker;

$factory->define(Sprint::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'project_id' => factory('App\Project')->create()->id
    ];
});
