<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DefinitionOfDoneItem;
use Faker\Generator as Faker;

$factory->define(DefinitionOfDoneItem::class, function (Faker $faker) {
    return [
        'text' => $faker->text,
        'definition_of_done_id' => factory('App\DefinitionOfDone')->create()->id
    ];
});
