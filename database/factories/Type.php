<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\Types;
use App\Type;
use Faker\Generator as Faker;

$factory->define(Type::class, function (Faker $faker) {
    return [
        'type' => Types::USER_STORY

    ];
});
