<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Backlog;
use Faker\Generator as Faker;

$factory->define(Backlog::class, function (Faker $faker) {
    return [
        'project_id' => factory('App\Project')->create()->id
    ];
});
