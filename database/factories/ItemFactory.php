<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->text,
        'story_points' => $faker->randomDigit,
        'business_value' => $faker->randomDigit,
        'state' => 0,
        'type_id' => factory('App\Type')->create()->id,
        'backlog_id' => factory('App\Backlog')->create()->id,
        'sprint_id' => factory('App\Sprint')->create()->id,
        'user_id' => factory('App\User')->create()->id
    ];
});
