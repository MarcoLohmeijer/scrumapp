<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetrospectiveItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retrospective_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');
            $table->boolean('type');

            $table->bigInteger('sprint_id')->unsigned()->index();
            $table->foreign('sprint_id')->references('id')->on('sprints');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retrospective_items');
    }
}
