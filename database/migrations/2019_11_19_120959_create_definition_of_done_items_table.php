<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefinitionOfDoneItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('definition_of_done_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');

            $table->bigInteger('definition_of_done_id')->unsigned()->index();
            $table->foreign('definition_of_done_id')->references('id')->on('definition_of_dones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_definition__of__done__items');
    }
}
