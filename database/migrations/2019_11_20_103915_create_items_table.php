<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->double('story_points')->nullable();
            $table->integer('business_value')->nullable();
            $table->integer('state')->default(0);

            $table->bigInteger('type_id')->unsigned()->index()->nullable();
            $table->foreign('type_id')->references('id')->on('types');

            $table->bigInteger('backlog_id')->unsigned()->index();
            $table->foreign('backlog_id')->references('id')->on('backlogs');

            $table->bigInteger('sprint_id')->unsigned()->index()->nullable();
            $table->foreign('sprint_id')->references('id')->on('sprints');

            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
