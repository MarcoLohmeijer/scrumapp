@servers(['web' => 'marco@adsd2019.clow.nl'])

@setup
    $gitUrl = 'git@gitlab.com:MarcoLohmeijer/scrumapp.git';
    $branch = (!empty($branch)) ? $branch : 'master';
    $path = '/home/marco/public_html/scrumapp';
@endsetup

@task('deploy:cold')
    cd {{$path}}
    git init
    git remote add origin {{$gitUrl}}
    git pull origin {{$branch}}

    composer install
    npm install

    php artisan storage:link

    php artisan migrate
@endtask

@task('deploy')
    cd {{$path}}
    git pull origin {{$branch}}

    composer install
    npm install

    php artisan storage:link

    php artisan migrate

    chown marco:www-data -R .
    chmod 775 -R .
@endtask
