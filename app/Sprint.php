<?php

namespace App;

use App\Http\Controllers\RetrospectiveItemsController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sprint extends Model
{
    use Notifiable;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'start_date', 'end_date',
    ];

    /**
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * @var array
     */
    protected $casts = [
    ];

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * @return HasMany
     */
    public function retrospective_items()
    {
        return $this->hasMany(Retrospective_items::class);
    }

    /**
     * @return HasMany
     */
    public function sprintreview()
    {
        return $this->hasMany(SprintReview::class);
    }
}
