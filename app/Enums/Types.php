<?php

namespace App\Enums;

class Types
{
    const BUG = 'bug';
    const USER_STORY = 'user_story';
    const SPIKE = 'spike';
    const EPIC = 'epic';

    const TYPES = [
        self::BUG,
        self::USER_STORY,
        self::SPIKE,
        self::EPIC
    ];
}
