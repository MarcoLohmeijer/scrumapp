<?php

namespace App\Enums;

class Roles
{
    const MAINTAINER = 'maintainer';
    const REPORTER = 'reporter';
    const DEVELOPER = 'developer';
    const GUEST = 'guest';

    const ROLES = [
        self::MAINTAINER,
        self::REPORTER,
        self::DEVELOPER,
        self::GUEST
    ];
}
