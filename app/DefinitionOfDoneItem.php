<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DefinitionOfDoneItem extends Model
{
    /**
     * @return BelongsTo
     */
    public function definitionOfDone()
    {
        return $this->belongsTo(DefinitionOfDone::class);
    }
}
