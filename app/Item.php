<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Item extends Model
{

    /**
     * @return BelongsTo
     */
    public function sprint()
    {
        return $this->belongsTo(Sprint::class);
    }

    /**
     * @return BelongsTo
     */
    public function backlog()
    {
        return $this->belongsTo(Backlog::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * @return HasMany
     */
    public function childItems()
    {
        return $this->hasMany(ChildItem::class);
    }
}
