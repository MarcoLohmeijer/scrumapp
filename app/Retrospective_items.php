<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class retrospective_items extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sprint()
    {
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }
}
