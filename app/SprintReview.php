<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SprintReview extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sprint()
    {
        return $this->belongsTo(Sprint::class, 'sprint_id');
    }
}
