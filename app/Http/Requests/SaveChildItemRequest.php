<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveChildItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_item' => 'required|integer',
            'name' => 'required|string',
            'description' => 'string',
            'user' => 'required|integer',
            'story_points' => 'required|integer',
            'business_value' => 'required|integer',
            'type' => 'required|integer',
        ];
    }
}
