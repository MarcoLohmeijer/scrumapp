<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSprintReviewRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'head' => 'required|string',
            'text' => 'required|string',
            'type' => 'required|int'
        ];
    }
}
