<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'string',
            'user_id' => 'required|int',
            'story_points' => 'required|int',
            'business_value'  => 'required|int',
            'type_id' => 'required|int',
            'sprint_id' => 'required|int',
            'page' => 'int'
        ];
    }
}
