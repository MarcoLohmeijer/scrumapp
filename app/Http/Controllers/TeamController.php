<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTeamRequest;
use App\Project;
use App\Team;
use App\User;
use App\Role;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TeamController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $users = User::all();
        $teams = Team::all();

        return view('teamOverview', [
            'users' => $users,
            'teams' => $teams
        ]);
    }

    /**
     * @param Project $project
     * @param CreateTeamRequest $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Project $project, CreateTeamRequest $request)
    {
        $team = new Team();
        $team->name = $request->get('name');
        $team->description = $request->get('description');
        $team->save();

        $team->users()->attach($request->get('users'));

        if ($request->get('addToProject')) {
            $project->teams()->attach($team);
        }

        return redirect()->back()->with('success','Team created successfully!');
    }

    public function create()
    {

    }

    public function show()
    {

    }

    /**
     * @param Team $team
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Team $team)
    {
        $team->delete();

        return redirect('/team')->with('warning','Team deleted successfully!');
    }

    /**
     * @param Team $team
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Team $team)
    {
        $team->name = request('name');
        $team->description = request('description');
        $team->save();

        $team->users()->attach(request('users'));

        return redirect('/team/'. $team->id . '/edit')->with('info','Team updated successfully!');
    }

    /**
     * @param Team $team
     *
     * @return Factory|View
     */
    public function edit(Team $team)
    {
        $users = User::whereNotIn('id', function($query) use($team) {
            $query->select('user_id')
                ->from('user_teams')
                ->where('user_teams.team_id', $team->id);
        })->get();
        $roles = Role::all();

        return view('teamEdit', compact('team', 'users', 'roles'))
            ->with('info','Team updated successfully!');
    }

    /**
     * @param Team $team
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function updateRole(Team $team, User $user)
    {
        $team->users()->updateExistingPivot($user->id, ['role_id' => request('role_id')]);

        return redirect('/team/'. $team->id . '/edit')
            ->with('info','Team updated successfully!');
    }


    /**
     * @param Team $team
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function deleteUser(Team $team, User $user)
    {
        $user->teams()->detach($team->id);

        return redirect('/team/'. $team->id . '/edit')->with('warning','User deleted from team successfully!');
    }

    /**
     * @return Factory|View
     */
    public function getAllJoinedTeams()
    {
        $user = Auth::user();
        $teams = $user->teams()->get();

        return view('teams.allTeamsOverview',
            compact('user', 'teams'));
    }
}
