<?php

namespace App\Http\Controllers;

use App\Backlog;
use App\Http\Requests\AddTeamToProjectRequest;
use App\Http\Requests\CreateProjectRequest;
use App\Project;
use App\Team;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ProjectController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('projects', ['projects' => Project::all()]);
    }

    /**
     * @param CreateProjectRequest $request
     *
     * @return RedirectResponse
     */
    public function store(CreateProjectRequest $request)
    {
        $project = new Project();
        $project->name = $request->get('name');
        $project->description = $request->get('description');
        $project->save();

        $productBacklog = new Backlog();
        $productBacklog->project_id = $project->id;
        $productBacklog->save();

        return redirect()->route('project.show', ['project' => $project->id])
            ->with('success','Project created successfully!');;
    }

    public function create()
    {

    }

    /**
     * @param Project $project
     *
     * @return Factory|View
     */
    public function show(Project $project)
    {
        $teams = Team::whereNotIn('id', function($query) use($project) {
            $query->select('team_id')
                ->from('project_teams')
                ->where('project_teams.project_id', $project->id);
        })->get();
        $backlog = $project->backlog()->first();
        $sprints = $project->sprints()->get();
        $definitionOfDones= $project->definitionOfDones()->get();
        $users = User::all();

        return view('projectOverview',
            compact('project', 'teams', 'backlog', 'sprints', 'definitionOfDones', 'users'));
    }

    public function destroy()
    {

    }

    public function update()
    {

    }

    public function edit()
    {

    }

    /**
     * @param AddTeamToProjectRequest $request
     * @param Project $project
     *
     * @return RedirectResponse
     */
    public function addTeams(AddTeamToProjectrequest $request, Project $project)
    {
        $project->teams()->attach($request->get('teams'));

        return back()->with('success','Team added successfully!');;
    }

    /**
     * @param Project $project
     * @param Team $team
     *
     * @return RedirectResponse
     */
    public function deleteTeam(Project $project, Team $team)
    {
        $project->teams()->detach($team->id);

        return back()->with('warning','Team deleted successfully!');;
    }
}
