<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateBoardItemRequest;
use App\Item;
use App\Sprint;
use App\Project;
use App\Type;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class ScrumController extends Controller
{
    /**
     * @param Project $project
     * @param Sprint $sprint
     *
     * @return Factory|View
     */
    public function index(Project $project, Sprint $sprint)
    {
        $items = $sprint->items()->get();
        $sprints = $project->sprints()->get();
        $teams = $project->teams()->get();
        $types = Type::all();

        //get users for this project
        $users = [];
        foreach ($teams as $team) {
            $team_users = User::whereIn('id', function($query) use($team) {
                $query->select('user_id')
                    ->from('user_teams')
                    ->where('user_teams.team_id', $team->id);
            })->get()->toArray();
            $users = array_merge($users, $team_users);
        }

        return view('scrum',
            compact('project', 'sprint', 'items', 'users', 'teams', 'types', 'sprints'));
    }

    /**
     * @param UpdateBoardItemRequest $request
     */
    public function updateItem(UpdateBoardItemRequest $request)
    {
        if ($request->get('action') === "move") {
            $id = substr($request->get("backlogId"), -1);
            $find = Item::find($id);
            $find->state = $request->get("state");
            $find->save();

            print("OK");
            return;
        }

        print("NOK");
    }
}
