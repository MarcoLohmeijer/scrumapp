<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSprintReviewRequest;
use App\Http\Requests\UpdateSprintReviewRequest;
use App\Project;
use App\SprintReview;
use Illuminate\Http\Request;
use App\Sprint;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

class SprintReviewController extends Controller
{
    /**
     * @param Project $project
     * @param Sprint $sprint
     * @return Factory|View
     */
    public function index(Project $project, Sprint $sprint)
    {
        $SprintReview = $sprint->SprintReview()->get();
        $finishedItems = $sprint->items()->where('state', '=', 4)->get();
        $unfinishedItems = $sprint->items()->where('state', '!=', 4)->get();

        $businessValueUnfinished = 0;
        $storyPointsUnfinished = 0;
        $businessValueFinished = 0;
        $storyPointsFinished = 0;

        foreach ($unfinishedItems as $item) {
            $businessValueUnfinished += $item->business_value;
            $storyPointsUnfinished += $item->story_points;
        }

        foreach ($finishedItems as $item) {
            $businessValueFinished += $item->business_value;
            $storyPointsFinished += $item->story_points;
        }


        return view('sprintReview',
            compact('sprint', 'project', 'SprintReview', 'storyPointsFinished', 'businessValueFinished',
            'businessValueUnfinished', 'storyPointsUnfinished'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Sprint $sprint
     * @param CreateSprintReviewRequest $request
     * @return RedirectResponse
     */
    public function store(Sprint $sprint, CreateSprintReviewRequest $request)
    {
        $sprint_reviews = new SprintReview();

        $sprint_reviews->head = $request->get('head');
        $sprint_reviews->text = $request->get('text');
        $sprint_reviews->type = $request->get('type');
        $sprint_reviews->sprint_id = $sprint->id;
        $sprint_reviews->save();

        return redirect()->back()->with('success','Sprint Review created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SprintReview  $sprintReview
     * @return \Illuminate\Http\Response
     */
    public function show(SprintReview $sprintReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SprintReview  $sprintReview
     * @return \Illuminate\Http\Response
     */
    public function edit(SprintReview $sprintReview)
    {
        //
    }

    /**
     * @param SprintReview $sprint_reviews
     * @param UpdateSprintReviewRequest $request
     * @return RedirectResponse
     */
    public function update(SprintReview $sprint_reviews, UpdateSprintReviewRequest $request)
    {
        $sprint_reviews->head = $request->get('head');
        $sprint_reviews->text = $request->get('text');
        $sprint_reviews->type = $request->get('type');
        $sprint_reviews->sprint_id = $request->get('sprintId');
        $sprint_reviews->save();

        return redirect()->back()->with('info','Sprint Review updated successfully!');
    }

    /**
     * @param SprintReview $sprint_reviews
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(SprintReview $sprint_reviews)
    {
        $sprint_reviews->delete();

        return redirect()->back()->with('warning','Sprint Review deleted successfully!');;
    }
}
