<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductBacklogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('productBacklog');
    }
}
