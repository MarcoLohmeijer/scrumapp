<?php

namespace App\Http\Controllers;

use App\ChildItem;
use App\Http\Requests\SaveChildItemRequest;
use App\Item;
use App\Sprint;
use App\Type;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ChildItemController extends Controller
{
    /**
     * @param $id
     * @param UpdateChildItemRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateChildItem($id, UpdateChildItemRequest $request)
    {
        $childItem =ChildItem::find($id);
        $childItem->title = $request->get('name');
        $childItem->description = $request->get('description');
        $childItem->user_id = $request->get('user');
        $childItem->story_points = $request->get('story_points');
        $childItem->business_value = $request->get('business_value');
        $childItem->type_id = $request->get('type');

        $childItem->save();
        return redirect()->back()->with('info','Child Item updated successfully!');
    }

    /**
     * @return Factory|View
     */
    public function showAddChildItemForm()
    {
        {
            $parentItems = Item::all();
            $users = User::all();
            $allUsers= User::all();
            $types = Type::all();
            $sprints = Sprint::all();

            return view('addChildItemForm', compact('types','users', 'sprints', 'allUsers', 'parentItems'));
        }
    }

    /**
     * @param SaveChildItemRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveItem(SaveChildItemRequest $request)
    {
        {
            $item = new ChildItem();
            $item->item_id = $request->get('parent_item');
            $item->title = $request->get('name');
            $item->description = $request->get('description');
            $item->user_id = $request->get('user');
            $item->story_points = $request->get('story_points');
            $item->business_value = $request->get('business_value');
            $item->type_id = $request->get('type');
            $item->save();

            return redirect()->back()->with('success','Child Item created successfully!');;
        }
    }

    /**
     * @return Factory|View
     */
    public function showDeleteChildItem()
    {
        $childItems = ChildItem::all();
        return view('deleteChildItemForm', compact('childItems') );
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showEditChildItemForm($id)
    {
        $items = ChildItem::find($id);
        $parentItems = Item::all();
        $allUsers = User::all();
        $users = User::where('id', $items->user_id)->first();
        $types = Type::all();

        return view('editChildItem', [
            'post' => Item::where('id', $id)->first()
        ], compact('types','users', 'items', 'id', 'parentItems' , 'allUsers'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteChildItem($id)
    {
        $childItem = ChildItem::find($id);
        $childItem->delete();
        return redirect()->back()->with('warning','Child Item deleted successfully!');;
    }

    /**
     * @return Factory|View
     */
    public function showChildItems()
    {
        $childBacklogItems = ChildItem::all();
        return view('childItemShow', compact('childBacklogItems'));
    }
}
