<?php

namespace App\Http\Controllers;

use App\Backlog;
use App\Project;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BacklogController extends Controller
{
    /**
     * @param Project $project
     *
     * @return Factory|View
     */
    public function index(Project $project)
    {
        $backlogItems = $project->backlog->items()->get();

        return view('backlogs.index', compact( 'backlogItems', 'project'));
    }
}
