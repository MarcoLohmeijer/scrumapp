<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRetrospectiveItemRequest;
use App\Http\Requests\UpdateRetrospectiveItemRequest;
use App\Project;
use App\Retrospective_items;
use App\Sprint;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

class RetrospectiveItemsController extends Controller
{
    /**
     * @param Project $project
     * @param Sprint $sprint
     * @return Factory|View
     */
    public function index(Project $project, Sprint $sprint)
    {
        $retrospective_items = $sprint->retrospective_items()->get();
        return view('retrospectiveItems', compact( 'sprint', 'project','retrospective_items'));
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * @param Sprint $sprint
     * @param CreateRetrospectiveItemRequest $request
     * @return RedirectResponse
     */
    public function store(Sprint $sprint, CreateRetrospectiveItemRequest $request)
    {
        $retrospective_item = new Retrospective_items();

        $retrospective_item->text = $request->get('text');
        $retrospective_item->type = $request->get('type');
        $retrospective_item->sprint_id = $sprint->id;
        $retrospective_item->save();

        return redirect()->back()->with('success','Retrospective Item created successfully!');
    }

    /**
     * @param Retrospective_items $retrospective_items
     */
    public function show(retrospective_items $retrospective_items)
    {
        //
    }

    /**
     * @param Retrospective_items $retrospective_items
     */
    public function edit(retrospective_items $retrospective_items)
    {
        //
    }

    /**
     * @param Retrospective_items $retrospective_items
     * @param UpdateRetrospectiveItemRequest $request
     * @return RedirectResponse
     */
    public function update(Retrospective_items $retrospective_items, UpdateRetrospectiveItemRequest $request)
    {

        $retrospective_items->text = $request->get('retroText');
        $retrospective_items->type = $request->get('retroType');
        $retrospective_items->sprint_id = $request->get('sprintId');

        $retrospective_items->save();

        return redirect()->back()->with('info','Retrospective Item updated successfully!');
    }

    /**
     * @param Retrospective_items $retrospective_items
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Retrospective_items $retrospective_items)
    {
        $retrospective_items->delete();

        return redirect()->back()->with('warning','Retrospective Item deleted successfully!');
    }
}
