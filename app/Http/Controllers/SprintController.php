<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSprintRequest;
use App\Http\Requests\UpdateSprintRequest;
use App\Sprint;
use App\Project;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

class SprintController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $sprints = Sprint::all();
        return view('sprintsOverview', ['sprints' => $sprints]);
    }

    /**
     * @param Project $project
     * @param CreateSprintRequest $request
     *
     * @return RedirectResponse
     */
    public function store(Project $project, CreateSprintRequest $request)
    {
        $sprint = new Sprint();

        $sprint->name = $request->get('name');
        $sprint->start_date = $request->get('startDate');
        $sprint->end_date = $request->get('finishDate');
        $sprint->project_id = $project->id;

        $sprint->save();

        return redirect()->back()->with('success','Sprint created successfully!');;
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('sprintAanmaken');
    }

    /**
     * @param Sprint $sprint
     *
     * @return Factory|View
     */
    public function show(Sprint $sprint)
    {
        $projects = $sprint->project()->first();

        return view('sprintAanmaken',compact('sprint', 'projects'));
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function destroy(Project $project, Sprint $sprint)
    {
        $sprint->delete();

        return redirect()->back()->with('warning','Sprint deleted successfully!');;
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     * @param UpdateSprintRequest $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Project $project, Sprint $sprint, UpdateSprintRequest $request)
    {
        $sprint->name = $request->get('name');
        $sprint->start_date = $request->get('startDate');
        $sprint->end_date = $request->get('endDate');

        $sprint->save();

        return redirect()->back()->with('info','Sprint updated successfully!');;
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     *
     * @return Factory|View
     */
    public function edit(Project $project, Sprint $sprint)
    {
        return view('sprintEdit', compact('sprint', 'project'));
    }
}
