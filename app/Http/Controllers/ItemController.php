<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Item;
use App\Project;
use App\Sprint;
use App\Type;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ItemController extends Controller
{
    public function index()
    {

    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     * @param CreateItemRequest $request
     *
     * @return RedirectResponse
     */
    public function store(Project $project, Sprint $sprint, CreateItemRequest $request)
    {
        $item = new Item();
        $item->title = $request->get('name');
        $item->description = $request->get('description');
        $item->user_id = $request->get('user');
        $item->story_points = $request->get('story_points');
        $item->business_value = $request->get('business_value');
        $item->type_id = $request->get('type');
        $item->sprint_id = $sprint->id;
        $item->backlog_id = $project->backlog()->first()->id;

        $item->save();

        return redirect()->back()->with('success','Item created successfully!');;
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     *
     * @return Factory|View
     */
    public function create(Project $project, Sprint $sprint)
    {
        $users = User::all();
        $types = Type::all();
        $sprints = Sprint::all();

        return view('addItem',
            compact('types', 'users', 'sprints', 'project'));
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     * @param Item $item
     * @param UpdateItemRequest $request
     *
     * @param $pageValue
     * @return RedirectResponse
     */
    public function update(Project $project, Sprint $sprint, Item $item, UpdateItemRequest $request)
    {
        $item->title = $request->get('title');
        $item->description = $request->get('description');
        $item->user_id = $request->get('user_id');
        $item->story_points = $request->get('story_points');
        $item->business_value = $request->get('business_value');
        $item->type_id = $request->get('type_id');
        $item->sprint_id = $request->get('sprint_id');
        $item->backlog_id = $project->backlog()->first()->id;

        $item->save();
        if ($request->get('page') == 9) {
            return redirect()->route('project.backlog.index', [
                'project' => $project->id,
            ])->with('info', 'Item updatet successfully!');
        }

        return redirect()->route('project.sprint.board.index', [
            'project' => $project->id,
            'sprint' => $sprint->id,
        ])->with('info','Item updatet successfully!');
    }

    public function show(Project $project, Sprint $sprint, Item $item)
    {
        return view('itemShow',
            compact('project', 'sprint', 'item'));
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();

        return redirect()->back()->with('warning','Item deleted successfully!');;
    }

    /**
     * @param Project $project
     * @param Sprint $sprint
     * @param Item $item
     *
     * @return Factory|View
     */
    public function edit(Project $project, Sprint $sprint, Item $item)
    {
        $users = User::where('id', $item->user_id)->first();
        $allUsers = User::all();
        $types = Type::all();
        $sprints = Sprint::all();

        return view('editItem',
            compact('types', 'users', 'project', 'sprints', 'allUsers', 'item', 'sprint'));
    }

    /**
     * @return Factory|View
     */
    public function getAssignedItems()
    {
        $user = Auth::user();
        $items = $user->items()->get();

        return view('items.assignedItems',
            compact('items', 'user'));
    }
}
