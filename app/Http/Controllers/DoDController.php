<?php

namespace App\Http\Controllers;

use App\DefinitionOfDone;
use App\DefinitionOfDoneItem;
use App\Http\Requests\CreateDoDItem;
use App\Http\Requests\CreateDoDItemRequest;
use App\Http\Requests\CreateDoDRequest;
use App\Http\Requests\EditDoDItem;
use App\Http\Requests\UpdateDoDItem;
use App\Project;
use App\Team;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DoDController extends Controller
{
    /**
     * @param Project $project
     * @return Factory|View
     */
    public function index(Project $project)
    {
        $definitionOfDones = $project->definitionOfDones()->get();
        $teams = $project->teams()->get();

        return view('definitionOfDone.definitionOfDoneOverview',
            compact('project', 'definitionOfDones', 'teams'));
    }

    /**
     * @param Project $project
     * @param CreateDoDRequest $request
     *
     * @return RedirectResponse
     */
    public function store(Project $project, CreateDoDRequest $request)
    {
        $dod = new DefinitionOfDone();
        $dod->project_id = $project->id;
        $dod->team_id = $request->get('team_id');
        $dod->save();

        return redirect()->back()->with('success','DoD Item created successfully!');;
    }

    public function create()
    {

    }

    public function destroy(DefinitionOfDoneItem $definitionOfDoneItem)
    {

        $definitionOfDoneItem->delete()->with('warning','DoD Item deleted successfully!');;

        return back();
    }

    public function update(DefinitionOfDoneItem $definitionOfDoneItem)
    {
        $definitionOfDoneItem->text = request('text');
        $definitionOfDoneItem->save();

        return view('definitionOfDone')->with('info','DoD Item updated successfully!');;
    }

    /**
     * @param Project $project
     * @param DefinitionOfDone $definitionOfDone
     *
     * @return Factory|View
     */
    public function show(Project $project, DefinitionOfDone $definitionOfDone)
    {
        $dodItems = $definitionOfDone->definitionOfDoneItems()->get();
        $team = $definitionOfDone->team()->first();

        return view('definitionOfDone',
            compact('project', 'team', 'definitionOfDone', 'dodItems'));
    }

    public function edit()
    {

    }

    /**
     * @param DefinitionOfDone $definitionOfDone
     * @param CreateDoDItemRequest $request
     *
     * @return RedirectResponse
     */
    public function createDodItem(DefinitionOfDone $definitionOfDone, CreateDodItemRequest $request)
    {
        $dodItem = new DefinitionOfDoneItem;
        $dodItem->text = $request->get('text');
        $dodItem->definition_of_done_id = $definitionOfDone->id;
        $dodItem->save();

        return redirect()->back()->with('success','DoD Item created successfully!');;
    }
}
