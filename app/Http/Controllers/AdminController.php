<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAdminOverview()
    {
        return view('adminOverview');
    }
}
