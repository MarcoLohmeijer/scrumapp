<?php

namespace App;

use App\Http\Controllers\RetrospectiveItemsController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Project extends Model
{
    /**
     * @return BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class, 'project_teams');
    }

    /**
     * @return HasMany
     */
    public function definitionOfDones()
    {
        return $this->hasMany(DefinitionOfDone::class);
    }

    /**
     * @return HasOne
     */
    public function backlog()
    {
        return $this->hasOne(Backlog::class, 'project_id');
    }

    /**
     * @return HasMany
     */
    public function sprints()
    {
        return $this->hasMany(Sprint::class);
    }
}
