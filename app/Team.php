<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Team extends Model
{
    /** @var array */
    protected $fillable = ['name', 'description', 'user_id'];

    /**
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, "user_teams")->withPivot('role_id');
    }

    /**
     * @return BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_teams');
    }

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, "user_teams");
    }

    /**
     * @return HasMany
     */
    public function definitionOfDones()
    {
        return $this->hasMany(DefinitionOfDone::class);
    }
}
