<?php

namespace App\Console\Commands;

use App\Enums\Types;
use App\Type;
use Illuminate\Console\Command;

class SeedItemTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with the items types';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(Types::TYPES as $type) {
            $newType = new Type;
            $newType->type = $type;
            $newType->save();
        }
        $this->line("Successful");
    }
}
