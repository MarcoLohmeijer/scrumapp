<?php

namespace App\Console\Commands;

use App\Enums\Roles;
use App\Role;
use Illuminate\Console\Command;

class SeedRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:roles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed database with roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Roles::ROLES as $role) {
            $newRole = new Role();
            $newRole->role = $role;
            $newRole->save();
        }
        $this->line("Successful");

    }
}
