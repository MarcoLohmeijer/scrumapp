<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class SeedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:users {value=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed random users into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = intval($this->argument('value'));
        factory(User::class, $amount)->create();
        $this->line('successful');
    }
}
