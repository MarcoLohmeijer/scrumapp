###ScrumApp

Om te beginnen met dit project te werken moet je de volgende programma's hebben:
* composer
* PHP 7.3+

Als je het cloned altijd eerst `composer install` doen zodat de packages ingeladen worden.
dan moet je `npm install` doen.

Om de front end goed te krijgen altijd voor de zekerheid `npm run dev` doen. en bij elke 
verandering in css of js kan je `npm run dev` of `npm run watch` doen.

Om de database te seeden met item types doe je de command `php artisan seed:types`

Om de role te seeden doe de command `php artisan seed:roles`

Om random users toe te voegen kan je de command `php artisan seed:users {amount}`
