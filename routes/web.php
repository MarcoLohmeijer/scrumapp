<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//users
Auth::routes();
Route::get('/account_overview', 'AccountController@showAccountOverview');

//home
Route::get('/', 'ProjectController@index')->name('home')->middleware('auth');
Route::get('/home', 'ProjectController@index')->name('home')->middleware('auth');

//child item
Route::get('/addchilditem', 'ChildItemController@showAddChildItemForm');
Route::post('/addchilditem', 'ChildItemController@saveItem')->name('saveItem');
Route::get('/edit_child_item/{id}', 'ChildItemController@showEditChildItemForm');
Route::post('/edit_child_item/{id}', 'ChildItemController@updateChildItem')->name('updateChildItem');
Route::get('/delete_child_item', 'ChildItemController@showDeleteChildItem');
Route::post('/delete_child_item/delete/{id}', 'ChildItemController@deleteChildItem')->name('deleteChildItem');
Route::get('/show_child_items', 'ChildItemController@showChildItems');

//Project
Route::resource('project', 'ProjectController')->middleware('auth');
Route::post('/project/{project}/add_teams','ProjectController@addTeams')->name('project.addTeam')->middleware('auth');
Route::delete('/project/{project}/delete_team/{team}','ProjectController@deleteTeam')->name('project.deleteTeam')->middleware('auth');

//Team
Route::resource('project.team', 'TeamController')->middleware('auth');
Route::delete('project/{project}/team/{team}/user/{user}', 'Teamcontroller@deleteUser')->name('team.deleteUser')->middleware('auth');
Route::put('project/{project}/team/{team}/updateRole/{user}', 'TeamController@updateRole')->name('team.updateRole')->middleware('auth');
Route::get('/teams', 'TeamController@getAllJoinedTeams')->name('teams')->middleware('auth');

//dod
Route::resource('project.dod', 'DoDController')->middleware('auth');
Route::post('project/{project}/dod/{definitionOfDone}/dod-item', 'DodController@createDodItem')->name('dod.dodItem.store')->middleware('auth');

//Sprint
Route::resource('project.sprint', 'SprintController')->middleware('auth');
Route::resource('sprints', 'SprintController')->middleware('auth');

//Item
Route::resource('project.sprint.item', 'ItemController')->middleware('auth');
Route::get('/items', 'ItemController@getAssignedItems')->name('items')->middleware('auth');

//Scrum
Route::resource('project.sprint.board', 'ScrumController')->only('index')->middleware('auth');

//Backlog
Route::resource('project.backlog', 'BacklogController')->only('index')->middleware('auth');

//ADMIN
Route::get('/admin_overview', 'AdminController@showAdminOverview');

//RetrospectiveItems
Route::resource('project.sprint.retrospectiveitem', 'RetrospectiveItemsController')->only('index')->middleware('auth');
Route::delete('retrospectiveitemsDestroy/{retrospective_items}', 'RetrospectiveItemsController@destroy')->name('destroyretrospectiveitem')->middleware('auth');
Route::post('retrospectiveitemStore/{sprint}', 'RetrospectiveItemsController@store')->name('storeretrospectiveitem')->middleware('auth');
Route::put('retrospectiveitemsUpdate/{retrospective_items}', 'RetrospectiveItemsController@update')->name('updateretrospectiveitem')->middleware('auth');

//SprintReview
Route::resource('project.sprint.sprintreview', 'SprintReviewController')->only('index')->middleware('auth');
Route::post('sprintreviewStore/{sprint}', 'SprintReviewController@store')->name('storesprintreview')->middleware('auth');
Route::delete('sprintreviewDestroy/{sprint_reviews}', 'SprintReviewController@destroy')->name('destroysprintreview')->middleware('auth');
Route::put('sprintreviewUpdate/{sprint_reviews}', 'SprintReviewController@update')->name('updatesprintreview')->middleware('auth');
